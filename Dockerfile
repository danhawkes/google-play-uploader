FROM python:2.7-alpine

RUN pip install google-api-python-client
COPY upload.py /upload.py
ENTRYPOINT ["usr/local/bin/python", "/upload.py"]




